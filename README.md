# 01_Dice_app

## Simple and fun app which display random numbers.

* Can display random numbers after tap on the button
* Can display random numbers after shaking device

## Visual Presentation

### Preview
![Static image](documentation/screen_1.png)
![Gif example](https://media.giphy.com/media/1qfDiaho7Mwd90tMaC/giphy.gif)



